<?xml version="1.0" encoding="UTF-8"?>
<tileset name="wood_lab" tilewidth="96" tileheight="96" tilecount="12" columns="4">
 <image source="wood_lab.png" width="384" height="288"/>
 <tile id="8">
  <animation>
   <frame tileid="8" duration="100"/>
   <frame tileid="0" duration="100"/>
   <frame tileid="1" duration="100"/>
   <frame tileid="2" duration="100"/>
   <frame tileid="3" duration="100"/>
   <frame tileid="4" duration="100"/>
   <frame tileid="5" duration="100"/>
   <frame tileid="6" duration="100"/>
   <frame tileid="7" duration="100"/>
  </animation>
 </tile>
</tileset>

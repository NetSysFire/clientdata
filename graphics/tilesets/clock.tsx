<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.4" tiledversion="1.4.1" name="clock" tilewidth="32" tileheight="64" tilecount="6" columns="6">
 <grid orientation="orthogonal" width="32" height="32"/>
 <image source="clock.png" width="192" height="64"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="300"/>
   <frame tileid="1" duration="210"/>
   <frame tileid="2" duration="210"/>
   <frame tileid="3" duration="210"/>
   <frame tileid="4" duration="210"/>
   <frame tileid="5" duration="300"/>
   <frame tileid="4" duration="210"/>
   <frame tileid="3" duration="210"/>
   <frame tileid="2" duration="210"/>
   <frame tileid="1" duration="210"/>
  </animation>
 </tile>
</tileset>
